import socket

s = socket.socket()
s.bind((socket.gethostname(), 60000))
s.listen(5)

while True:
    conn, addr = s.accept()
    data = conn.recv(1024)
    print('Server received', repr(data))

    filename = "README"
    f = open(filename)
    l = f.read(1024)

    while(l):
        conn.sendall(l)
        print 'Sent %s',repr(l)
        l = f.read(1024)

    f.close()

    print 'Done sending'
    conn.send('Thank you for connecting')
    conn.close()