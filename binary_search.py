def binary_search(a_list, item):
    first = 0
    last = len(a_list) - 1
    found = None

    while first <=last and not found:
        midpoint = (first + last) // 2
        if a_list[midpoint] == item:
            found = True
        elif item < a_list[midpoint]:
            last = midpoint -1
        else:
            first = midpoint + 1
    return found



# binary search with recursion
def binary_search_with_recursion(a_list, item):
    if not len(a_list):
        return False

    midpoint = len(a_list) // 2

    if item == a_list[midpoint]:
        return True

    elif item > a_list[midpoint]:
        binary_search_with_recursion(a_list[midpoint+1:], item)

    else:
        binary_search_with_recursion(a_list[:midpoint - 1], item)


if __name__=="__main__":
    test_list = [0, 1, 2, 8, 13, 17, 19, 32, 42,]
    print binary_search(test_list, 3)
    print binary_search(test_list, 13)

    test_list = [0, 1, 2, 8, 13, 17, 19, 32, 42,]
    print binary_search_with_recursion(test_list, 3)
    print binary_search_with_recursion(test_list, 13)