import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 12345))
s.listen(1)
conn, addr = s.accept()


print 'accept connection %s, %s' % addr

while True:
    data = conn.recv(1024)
    print 'Receive %s' % repr(data)
    if not data:
        break
    conn.sendall(data)

conn.close()
