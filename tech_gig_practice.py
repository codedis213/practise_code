def word_count(input1,input2,input3):
	#Write code here
	n = input1
	rows_element_list = input2
	sub_string = input3
	
	b = []

	for i in range(0, len(rows_element_list)):
		b.append([str(x).upper() for x in rows_element_list[i].split("#")])

	l = len(sub_string)
	counter = 0 

	for row in range(0, n):
		for col in range(0, n):
			if "".join(b[row][col: col+l]) == sub_string:
				counter += 1

			vd = b[row][col]
			dd = b[row][col]
			da = b[row][col]
			
			for i in range(1, len(sub_string)):
				
				if row+i < n:
					vd += b[row+i][col]

				if row+i< n and col+i < n:
					dd += b[row+i][col+i]

				if row-i >= 0 and col+i<n:
					da += b[row-i][col+i]

			if vd == sub_string:
				counter += 1

			if dd == sub_string:
				counter += 1

			if da == sub_string:
				counter += 1

	print counter



def collegecomparison(input1,input2,input3):

	n, c1, c2 = input1,input2,input3

	c1_dict = dict()
	c2_dict = dict()

	if c1.__len__() != n or c2.__len__() != n:
		return "Invalid"


	for i in range(0, c1.__len__()):
		if c1[i] < 0 or c2[i] < 0:
			return "Invalid"

		if c1_dict.has_key(c1[i]):
			c1_dict[c1[i]] += 1
		else:
			c1_dict[c1[i]]  = 1

		if c2_dict.has_key(c2[i]):
			c2_dict[c2[i]] += 1

		else:
			c2_dict[c2[i]] = 1


	if c2_dict.keys() != c1_dict.keys():
		return "Unequal"

	for key, val in c1_dict.iteritems():
		if c2_dict[key] != val:
			return "Unequal"

	return "Equal"


#  exam 1 >> q1
def getHeight(input1,input2):
	heightarray, persionnum = nput1,input2
	count = 0
	
	for i, Hi  in  enumerate(heightarray):
		xi = int(Hi[0])
		yi = int(Hi[2:])

		if not (xi >= 4 and xi <= 7 and yi >= 0 and yi <= 11):
			return -1

		for Hj in heightarray[i+1:]:
			xj = int(Hj[0])
			yj = int(Hj[2:])

			if not (xj >= 4 and xj <= 7 and yj >= 0 and yj <= 11):
				return -1

			if xi == xj:
				if yi > yj:
					count += 1
			elif xi > xj:
				count += 1

	return count


def minimumCost(input1,input2):
	costmatrix = [row.split("#") for row in input1]

	if (not costmatrix) or costmatrix.__len__() != input2:
		return "NA"

	rowpos = 0 
	colpos = 0 

	M = input2 -1 
	N = costmatrix[0].__len__() -1 

	s = ''
	totalcost = int(costmatrix[rowpos][rowpos])

	while (rowpos < M ) and (colpos < N ):

		rval = int(costmatrix[rowpos][colpos+1])
		bval = int(costmatrix[rowpos+1][colpos])
		dval = int(costmatrix[rowpos+1][colpos+1])
		
		if (bval < dval and bval < rval):
			totalcost += bval
			rowpos = rowpos +1
			s += "B"

		elif dval < rval:
			totalcost += dval
			rowpos += 1
			colpos += 1
			s += "D"

		else:
			totalcost += rval
			colpos += 1
			s += "R"

	if rowpos == M and colpos == N:
		s += "D"
		totalcost += int(costmatrix[rowpos][colpos])

	elif rowpos == M and colpos  == N -1:
		s += "R"
		totalcost += int(costmatrix[rowpos][colpos+1])

	elif rowpos == M-1 and colpos == N:
		s += "B"
		totalcost += int(costmatrix[rowpos+1][colpos])


	print "%s,%s" %(totalcost, s)


def requiredTetriminos(input1, input2, input3):
	
	tetris_dict = [[(0,0), (0,-1), (0, -2)], 
	[(0,0), (-1,0), (-2,0)], 
	[(0,0), (0,1),(-2,0)], 
	[(0,0),(-1,0),(-1,-1),(-1,-2)],
	[(0,0),(0,1),(-1,1),(-2,1)],
	[(0,0),(0,1),(-1,1),(-2,1)],
	[(0,0),(0,-1),(0,-2),(-1,-2)],
	[(0,0),(0,-1),(0,-2),(-1,-2)],
	[(0,0),(0,-1),(1,-1),(2,-1)],
	[(0,0),(-1,0),(0,-1),(0,-2)],
	[(0,0),(0,-1),(-1,-1),(-2,-1)],
	[(0,0),(-1,0),(-1,1),(-1,2)],
	[(0,0),(0,-1),(1,-1),(1,-2)],
	[(0,0),(-1,0),(-1,-1),(-2,-1)],
	[(0,0),(0,-1),(-1,1),(-1,2)],
	[(0,0),(1,0),(1,1),(2,1)],
	[(0,0),(-1,0),(0,-1),(0,-2)],
	[(0,0),(0,-1),(-1,-1),(-2,-1)],
	[(0,0),(0,1),(-1,0),(-2,0)],
	[(0,0),(0,1),(-1,0),(-2,0)],
	[(0,0),(-1,0),(-1,1),(-1,2)],
	[(0,0),(-1,0),(-2,0),(-2,-1)],
	[(0,0),(-1,0),(-1,-1),(-1,1)],
	[(0,0),(-1,0),(-2,0),(-1,-1)],
	[(0,0),(0,-1),(0,-2),(-1,-1)],
	[(0,0),(0,-1),(0,-2),(-1,-1)],
	[(0,0),(-1,0),(-2,0),(-1,1)],
	[(0,0),(0,-1),(-1,-1),(-1,0)],
	[(0,0),(0,-1),(-1,-1),(-1,-2)],
	[(0,0),(-1,0),(-1,1),(-2,1)]
	]

	lst_len = input3.__len__()

	for index in xrange(0 , lst_len):
		element = input3[lst_len -1 - index]

		if not element:
			row    = (int)(index / input1)
			column = index % input1

			found = False
			count = 0
			tetris_dict_len = len(tetris_dict)

			while (not found) and (count < tetris_dict_len):
				tetriminos = tetris_dict[count]	

				for r, c in tetriminos:













if __name__=="__main__":
	# word_count(input1,input2,input3):
	# print collegecomparison(7, [12, 11, 5, 2, 7, 5, 11], [5, 12, 5, 7, 11, 2, 11])
	# print interchangeable(["5#8", "5#7", "6#0", "5#7"], 4)
	# print interchangeable(["4#11", "6#0", "5#2", "6#1", "7#1", "5#11", "5#11", "5#10", "5#8"], 9)
	# minimumCost(["5#7#2#4", "1#8#1#3", "6#2#9#5", "1#6#2#8"], 4)
	requiredTetriminos(3, 3, [1,1,1,0,0,0,0,0,0])