import socket

s = socket.socket()
s.connect((socket.gethostname(), 60000))
s.sendall("hello server")

with open("receive_file", "wb") as f:
    print "file opened"
    while True:
        print "receive data..."
        data = s.recv(1024)

        print "data received %s " % data
        if not data:
            break
        f.write(data)

f.close()
print "successfully get file "
s.close()
print "connection closed"