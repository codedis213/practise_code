# Enter your code here. Read input from STDIN. Print output to STDOUT

string = raw_input().strip()
totalScore = len(string)*(len(string)+1)/2
scoreKevin = 0
vowels = ['A', 'E', 'I', 'O', 'U']
for i in range(len(string)):
    if string[i] in vowels:
        scoreKevin += len(string)-i
if scoreKevin>(totalScore-scoreKevin):
    print 'Kevin', scoreKevin
elif scoreKevin == (totalScore-scoreKevin):
    print 'Draw'
else:
    print 'Stuart', (totalScore-scoreKevin)