import socket
import time

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# host = socket.gethostname()
# port = 9999
serversocket.bind((socket.gethostname(), 9991))

serversocket.listen(5)

while True:
    clientsocket, addr = serversocket.accept()
    print clientsocket, addr

    print "Got a connection from %s, %s" % addr
    currentTime = time.ctime(time.time()) + "\r\n"

    clientsocket.send(currentTime.encode("ascii"))
    clientsocket.close()
