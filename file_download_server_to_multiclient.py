import socket
from threading import Thread
from SocketServer import ThreadingMixIn

# TCP_IP = 'localhost'
TCP_IP = socket.gethostbyaddr("your-ec2-public_ip")[0]
TCP_PORT = 9001
BUFFER_SIZE = 1024

class ClientThread(Thread):

    def __init__(self, ip, port, conn):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.conn = conn
        print " New thread started for "+ip+":"+str(port)

    def run(self):
        filename = "README"
        f = open(filename, "rb")
        while True:
            l = f.read(BUFFER_SIZE)
            while l:
                self.conn.send(l)
                l = f.read(BUFFER_SIZE)
            if not l:
                f.close()
                self.conn.close()
                break


tcpsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsocket.bind((TCP_IP, TCP_PORT))

threads = []

while True:
    tcpsocket.listen(5)
    print "Waiting for incoming connections..."
    (conn, (ip,port)) = tcpsocket.accept()
    print 'Got connection from ', (ip,port)
    newthread = ClientThread(ip,port,conn)
    newthread.start()
    threads.append(newthread)

for t in threads:
    t.join()