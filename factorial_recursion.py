# factorial using recursion 
def facto(n):
	if n<=0:
		return 1
	else:
		return n * facto(n-1)


# convert integer to  base 16 string 
def toStr(n,base):
	convertString = "0123456789ABCDEF"
	if n < base:
		return convertString[n]
	else:
		return  toStr(n//base,base) + convertString[n%base] 


# reverse a string 

def reverse(basic_str):
	if len(basic_str) < 1:
		return ""
	else:
		return basic_str[-1] + reverse(basic_str[:-1])

def removeWhite(s):
	s2 = ""
	for ch  in s: 
		if ch.isalpha():
			s2 += ch
	return s2


def isPal(s):
	s = removeWhite(s)
	return reverse(s) == s


if __name__=="__main__":
	# print facto(3)
	# print toStr(1453,16)
	# print reverse("i love india")
	print removeWhite("i love india")
	print isPal("mam  ")
