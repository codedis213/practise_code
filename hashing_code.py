def hash(a_string, table_size):
    sum = 0
    for ch in a_string:
        sum += ord(ch)

    return sum % table_size


if __name__ == "__main__":
    print hash("cat", 11)