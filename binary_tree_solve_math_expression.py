from binary_tree_node_refrence import BinaryTree
from stack import Stack
import operator

def build_parse_tree(fp_exp):
    fp_list = fp_exp.split()

    p_stack = Stack()

    e_tree = BinaryTree("")
    
    p_stack.push(e_tree)

    current_tree = e_tree

    for i in fp_list:
        if i == "(":
            current_tree.insert_left('')
            p_stack.push(current_tree)
            current_tree = current_tree.get_left_child()

        elif i not in ["+", "-", "/", "*", ")"]:
            current_tree.set_root_val(int(i))
            parent = p_stack.pop()
            current_tree = parent

        elif i in ["+", "-", "/", "*"]:
            current_tree.set_root_val(i)
            current_tree.insert_right('')
            p_stack.push(current_tree)
            current_tree = current_tree.get_right_child()

        elif i == ")":
            current_tree = p_stack.pop()

        else:
            raise ValueError

    return e_tree




# evaluate left and right leave and return the calculation

def evaluate(parse_tree):
    opers = {"+": operator.add, "-": operator.sub, "*": operator.mul, "/": operator.div}

    left = parse_tree.get_left_child()
    right = parse_tree.get_right_child()

    if left and right:
        fn = opers[parse_tree.get_root_val()]
        return fn(evaluate(left), evaluate(right))
    else:
        return parse_tree.get_root_val()


# same as above function for calculation
def postorder_eval(tree):
    opers = {"+": operator.add, "-": operator.sub, "*": operator.mul, "/": operator.truediv}
    res1 = None
    res2 = None

    if tree:
        res1 = postorder_eval(tree.get_left_child())
        res2 = postorder_eval(tree.get_right_child())
        if res1 and res2:
            return opers[tree.get_root_val()](res1, res2)
        else:
            return tree.get_root_val()



# for traversing continues looking for left child, than came back to respective root, ]
# then do same for left chile, then right

def preorder(tree):
    if tree:
        print  (tree.get_root_val())
        preorder(tree.get_left_child())
        preorder(tree.get_right_child())


# for traversing continues looking for right child, than came back to respective root, ]
# then do same for right chile, then left

def postorder(tree):
    if tree:
        postorder(tree.get_left_child())
        postorder(tree.get_right_child())
        print tree.get_root_val()


# for traversing
# search for left
# go to parent
# search for right

def inorder(tree):
    if tree:
        inorder(tree.get_left_child())
        print tree.get_root_val()
        inorder(tree.get_right_child())


# print tree expression
def print_exp(tree):
    str_val = ''
    if tree:
        str_val = "(" + print_exp(tree.get_left_child())
        str_val += str(tree.get_root_val())
        str_val += print_exp(tree.get_right_child())

    return str_val


if __name__=="__main__":
    pt = build_parse_tree("( ( 10 + 5 ) * 3 )")
    print "binary object"
    print pt
    print "expression evaluation"
    print evaluate(pt)
    print "expression evaluation via post order"
    print postorder_eval(pt)
    print "preorder traversal"
    preorder(pt)
    print "post order traversal"
    postorder(pt)
    print "inorder traversal"
    inorder(pt)
    print "print expression"
    print print_exp(pt)



